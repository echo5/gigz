$(document).on('turbolinks:load', function() {
    if ( $('.pagination').length ) {
        $('.conversation-box').bind('scroll', loadMessagesOnScroll);
    }
});

$(document).on('turbolinks:request-start', function() {
    $('.conversation-box').unbind('scroll');
});

function loadMessagesOnScroll(e) {
    var elem = $(e.currentTarget);
    if (elem.scrollTop() < 20) {
        console.log("top");
        var nextPage = $('.pagination .next_page a');
        var pagination = $('.pagination');
        if (!nextPage.parent().hasClass('disabled') && !elem.hasClass('loading')) {
            elem.addClass('loading');
            url = nextPage.attr('href');    
            $('.pagination').html("<i class='ion-ios-reload'></i> Loading previous messages...");
            $.get(url, null, function(data){
            }, 'script');
        }
    }
}

$(document).on('click', '.message-user', function(e) {
    e.preventDefault();
    var userName = $(this).data('user-name');
    var userId = $(this).data('user-id');
    var messageModal = $('#message-modal');
    messageModal.find('.user-name').html(userName);
    messageModal.find('#message_recipient_id').val(userId);
    messageModal.modal('show');
});