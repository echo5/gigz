$(document).on('change', '.quantity', function() {
    updateAmount($(this));
});

$(document).on('change', '.coupon-code', function() {
    
    var couponCheck = $(this).closest('form').find('.coupon-check');
    var amount = $(this).closest('form').find('.amount-value');
    
    $.ajax({
        method: 'POST',
        url: '/charges/check_coupon',
        data: { couponCode: $(this).val() },
        success: function(response) {
            amount.attr('data-value-discount', '').attr('data-percentage-discount', '');
            if (!$.trim(response)){  
                 couponCheck.html('Invalid coupon code.').removeClass('success').addClass('error');
            } else {
                var couponText = '';
                if ($.trim(response.percentage_discount)) {
                    couponText += response.percentage_discount + '% off ';
                    amount.attr('data-percentage-discount', response.percentage_discount);
                }
                if ($.trim(response.value_discount)) {
                    couponText += '$' + parseFloat(response.value_discount / 100).toFixed(2) + ' off';
                    amount.attr('data-value-discount', response.value_discount);                    
                }
                couponCheck.html(couponText).removeClass('error').addClass('success');
            }
            updateAmount(couponCheck);    
        }
    });
});

function updateAmount(element) {
    var form = element.closest('form');        
    var amount = form.find('.amount-value');
    var quantity = form.find('.quantity').val();
    var newAmount = parseInt(quantity) * parseFloat(amount.data('base'));
    if (amount.attr('data-value-discount')) {
        newAmount = newAmount - amount.data('valueDiscount');
    }
    if (amount.attr('data-percentage-discount')) {
        newAmount = newAmount * ( 1 - (amount.data('percentageDiscount') / 100) );
    }
    amount.html((newAmount / 100).toFixed(2));
}