$(document).on('turbolinks:load', function() {

  /**
  * Tooltips
  */
  $('[data-toggle="tooltip"]').tooltip();

  /**
  * Sticky elements
  */
  $(".stick").stick_in_parent({offset_top: 40});
  document.addEventListener("turbolinks:before-cache", function() {
    $(".stick").trigger("sticky_kit:detach");
  });  

  /**
   * Off canvas
   */
  $('[data-toggle="offcanvas"]').on('click', function () {
    $('.row-offcanvas').toggleClass('active')
  })

  /**
   * Invitation Code Omniauth
   */
  $('.provider').on('click', function (e) {
    var url = $(this).attr('href');
    if(window.location.href.indexOf("sign_up") > -1) {
      e.preventDefault();
      $('#invitation-code-modal').modal('show');
      $('#redirect_to').val(url);
    }
  });

  /**
   * Fix Ckeditor Turbolinks
   */
  document.addEventListener("turbolinks:before-cache", function() {
    $(".cke").remove();
  });  

})

