/*
 * Comment reply link
 */
$(document).on('click', '.comment-reply-link', function(e){
  e.preventDefault();
  var comment = $(this).closest('.comment');
  $('#comment-form-header').html('Reply to ' + comment.data('name'));
  $('#new_post_comment').find('#post_comment_parent_id').val(comment.data('id'));
  $('#comment-reply-cancel').show();
  $('html, body').animate({
      scrollTop: $('#comment-form-header').offset().top - 150
  }, 500);
});
$(document).on('click', '#comment-reply-cancel', function(e){
  e.preventDefault();
  $(this).hide();
  $('#comment-form-header').html('Leave a comment');
  $('#new_post_comment').find('#post_comment_parent_id').val('');
});