module PagesHelper
	def process_content(content)
		content.sub! '[[categories]]', categories

		content
	end

	def categories
		render 'categories/list'
	end

end
