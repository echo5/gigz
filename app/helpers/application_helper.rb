module ApplicationHelper
  def nav_link(link_text, link_path, sub_menu = nil, tooltip = nil, tooltip_position = nil, partial_path = false)
    if partial_path
      class_name = (request.fullpath.include? link_path) ? 'active' : ''			
    else
      class_name = (link_path == request.fullpath) ? 'active' : ''
    end

    content_tag(:li, :class => 'nav-item') do
      concat(link_to raw(link_text), link_path, :class => 'nav-link ' + class_name, 
        data: { title: tooltip, toggle: (tooltip ? 'tooltip' : nil), placement: tooltip_position }
      ) 
      if defined?(sub_menu) && !sub_menu.blank?
        concat(content_tag(:ul, class: 'dropdown-menu') do
          sub_menu.each do |item|
            concat(item)
          end
        end)
      end
    end
  end

  def body_class(classes)
    content_for :body_class, classes.to_s
  end


	def title(page_title)
	  content_for :title, page_title.to_s
	end

	def meta_description(desc = nil)
	  if desc.present?
		content_for(:meta_description, tag(:meta, name: 'description', content: desc ))
 	  end
  end
  
  def friendly_time(time)
    if time < Time.now - 30.days
      date = time.strftime('%b %e')
      date = date + time.strftime(', %Y') if time < Time.now - 1.year
    else
      date = time_ago_in_words(time) + ' ago'
    end
    return date
  end
  
end
