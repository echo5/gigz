class MessagePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def create?
    user and record.user == user
  end

  def update?
    false
  end
  
end
