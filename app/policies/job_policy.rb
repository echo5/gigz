class JobPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def create?
    user.postings_remaining? || user&.is_admin?
  end

  def edit?
    user == record || user&.is_admin?
  end

  def update?
    user == record || user&.is_admin?
  end

  def apply?
    user && !record.has_applied?(user) && record.poster != user
  end

  def show?
    return true
  end

  def my?
    user
  end

  def new?
    user && user.postings_remaining?
  end
end
