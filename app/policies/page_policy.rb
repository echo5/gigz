class PagePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def show?
    record.status == 'published' || (user && user.is_admin?)
  end
end
