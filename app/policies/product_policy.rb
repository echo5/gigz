class ProductPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.where(status: 'published')
    end
  end

  def show?
    user
  end
end
