class UserPolicy < ApplicationPolicy
  attr_reader :user, :post
  
  # def initialize(user, user_record)
  #   @user = user
  #   @user_record = user_record
  # end

  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def edit?
    user == record || user&.is_admin?
  end

  def update?
    user == record || user&.is_admin?
  end

  def connect?
    user != record && !user&.friends_with?(record)
  end

  def message?
    user && user != record
  end

  def endorse?
    user && user != record && !user&.has_endorsed?(record)    
  end

  def show?
    return true
  end
  
  def connections?
    true
  end

  def accept_connection?
    true
  end

  def remove_connection?
    true
  end

  def decline_connection?
    true
  end

end
