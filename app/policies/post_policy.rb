class PostPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.where(status: 'published')
    end
  end

  def show?
    user&.is_admin? || record.status == 'published'
  end

  def new?
    true
  end

  def create?
    user
  end

  def like?
    user 
  end

  def unlike?
    user
  end

end
