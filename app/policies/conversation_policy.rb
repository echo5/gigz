class ConversationPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def index?
    user
  end

  def show?
    user && user.is_admin? || record.recipient_id == user&.id || record.sender_id == user&.id
  end
end
