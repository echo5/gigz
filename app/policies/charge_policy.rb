class ChargePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def check_coupon?
    user
  end

  def create?
    user
  end
end
