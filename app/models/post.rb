class Post < ApplicationRecord
  belongs_to :user
  has_many :comments, class_name: 'PostComment', dependent: :destroy
  has_many :approved_comments, -> { where(status: 'approved').order(created_at: :asc) }, class_name: 'PostComment'
  has_many :approved_root_comments, -> { where(parent_id: nil).where(status: 'approved').order(created_at: :asc) }, class_name: 'PostComment'
  has_attached_file :image, :styles => {
  :small  => "500x300#",
  :large => "900x500#" }
  validates_attachment :image, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
  validates_presence_of :title, :content, :slug, :description, :user
  validates_uniqueness_of :slug
  acts_as_votable
  self.per_page = 20

  before_save :sanitize_content

  enum status: {
    draft: 0,
    pending: 1,
    published: 2,
  }

  def to_param
    "#{id}-#{self.title.parameterize}"
  end

  def sanitize_content
    self.content = Sanitize.fragment(content, Sanitize::Config.merge(Sanitize::Config::BASIC,
      :elements => Sanitize::Config::BASIC[:elements] + ['h2', 'h3', 'h4', 'h5', 'ul', 'ol', 'div', 'img'],
      :attributes => {
        'img' => %w[align alt border height src srcset width],
      },
      :protocols => {
        'img' => {'src'  => ['http', 'https', :relative]},
      },
      :remove_contents => true
    ))
  end
end
