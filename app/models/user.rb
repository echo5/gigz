class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :omniauth_providers => [:facebook, :google_oauth2]

  validates_presence_of :name, :email, :role
  validates_acceptance_of :terms
  validates :invitation_code, on: :create, presence: true
  validate :unused_invitation_code, on: :create
  attr_accessor :terms, :invitation_code

  has_friendship
  belongs_to :category, optional: true
  has_many :invitation_codes, dependent: :destroy, foreign_key: "owner_id"
  has_many :posts, dependent: :destroy
  has_many :portfolio_items, dependent: :destroy
  has_many :endorsements, inverse_of: :user, dependent: :destroy
  has_many :educations, inverse_of: :user, dependent: :destroy
  has_many :experiences, inverse_of: :user, dependent: :destroy
  has_many :jobs, foreign_key: :poster_id, dependent: :destroy
  has_many :job_applications, foreign_key: :applicant_id, dependent: :destroy
  has_many :connections, -> { where(friendships: {status: 2}) }, :through => :friendships, :source => :friend
  has_many :opened_conversations, class_name: "Conversation", foreign_key: "sender_id", dependent: :destroy
  has_many :received_conversations, class_name: "Conversation", foreign_key: "recipient_id", dependent: :destroy
  has_attached_file :avatar, :styles => {
  :thumb => "50x50#",
  :small  => "150x150#" }
  validates_attachment :avatar, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
  validates :tagline, length: {
    maximum: 15,
    tokenizer: lambda { |str| str.scan(/\w+/) },
    too_long: "must have at most %{count} words"
  }
  validates :elevator_pitch, length: {
    maximum: 150,
    tokenizer: lambda { |str| str.scan(/\w+/) },
    too_long: "must have at most %{count} words"
  }
  validates_presence_of :category_id #, :if => Proc.new { |user| user.role == 'talent' }

  accepts_nested_attributes_for :portfolio_items, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :educations, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :experiences, reject_if: :all_blank, allow_destroy: true

  after_commit :use_invitation  
  after_create :subscribe_to_mailer

  acts_as_voter

  # Scopes
  include Filterable
  scope :uname, -> (name) { where('lower(name) like lower(?)', "#{name}%") }
  scope :category, -> (category) { where category: category }
  scope :country, -> (country) { where country: country }
  default_scope { order('created_at DESC') }
  self.per_page = 21

	enum role: {
    talent: 0,
    influencer: 1,
    admin: 2,
  }
  
  PUBLIC_ROLES = [
    'talent'
  ]

  def render_avatar(size = 80)
    if avatar.exists?
      url = avatar.url(:small)
    else
      gravatar_id = Digest::MD5.hexdigest(email.downcase)
      url = "https://gravatar.com/avatar/#{gravatar_id}.png?s=#{size}&d=mm"
    end
    "<img src='#{url}' alt='#{name}' class='avatar' width='#{size}' height='#{size}'>".html_safe
  end

  def is_admin?
    role == 'admin'
  end

  def conversations
    Conversation.where("sender_id = ? OR recipient_id = ?", self.id, self.id)
  end

  def self.from_omniauth(auth, role, invitation_code = nil, category_id = nil)
    where(email: auth.info.email).first_or_create! do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      user.email = auth.info.email
      user.avatar = open(auth.info.image)
      user.password = Devise.friendly_token[0,20]
      user.confirmed_at = Time.now
      user.invitation_code = invitation_code
      user.role = role || 'talent'
      user.category_id = category_id
      user.skip_confirmation!
    end
  end

  def has_endorsed?(user)
    endorsement = Endorsement.where(endorser_id: self.id).where(user_id: user.id)
    return endorsement.count > 0
  end

  def postings_remaining?
    (fulltime_postings_remaining && fulltime_postings_remaining > 0) || (freelance_postings_remaining && freelance_postings_remaining > 0)
  end

  def unused_invitation_code
    invitation_code = InvitationCode.find_by_code(self.invitation_code)
    if !invitation_code
      errors.add(:invitation_code, "does not exist")
    elsif !invitation_code.user_id.nil?
      errors.add(:invitation_code, "has already been used")
    elsif invitation_code.role
      self.role = invitation_code.role
    else
      self.role = 'talent'
    end
  end

  def use_invitation
    invitation = InvitationCode.find_by_code(self.invitation_code)
    if invitation
      invitation.user = self
      invitation.save

      codes = (0...10).map { { code: SecureRandom.hex(15), owner: self } }
      invitation_codes = InvitationCode.create(codes)
      InvitationCodeMailer.send_codes(self,invitation_codes).deliver_now
    end
  end

  def first_name
    if name.split.count > 1
      name.split[0..-2].join(' ')
    else
      name
    end
  end

  def last_name
    if name.split.count > 1
      name.split.last
    end
  end

  def subscribe_to_mailer
    user = self
    begin
      Gibbon::Request.lists('8db999d57d').members.create(body: {email_address: user.email, status: "subscribed", merge_fields: {FNAME: user.first_name, LNAME: user.last_name}})
    rescue => ex
    end
  end

  def get_available_invitation_code
    invitations = self.invitation_codes.where(user_id: nil)
    if invitations.any?
      invitations.first
    else
      self.invitation_codes.create(code: SecureRandom.hex(15))
    end
  end

end
