class Endorsement < ApplicationRecord
  belongs_to :user
  belongs_to :endorser, class_name: 'User'
  validates_presence_of :user, :endorser, :body
  validate :not_self

  def not_self
    errors.add(:base, 'You cannot endorse yourself.') if user_id == endorser_id
  end

end
