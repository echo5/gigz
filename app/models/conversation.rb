class Conversation < ApplicationRecord
  belongs_to :sender, :foreign_key => :sender_id, class_name: 'User'
  belongs_to :recipient, :foreign_key => :recipient_id, class_name: 'User'
  has_many :messages, dependent: :destroy
  validates_uniqueness_of :sender_id, :scope => :recipient_id

  before_create :set_last_message_at

  scope :between, -> (sender_id,recipient_id) do
    where("(conversations.sender_id = ? AND conversations.recipient_id =?) OR (conversations.sender_id = ? AND conversations.recipient_id =?)", sender_id,recipient_id, recipient_id, sender_id)
  end

  def other_participant(current_user_id)
    return sender if sender_id != current_user_id
    recipient
  end

  def set_last_message_at
    self.last_message_at = Time.now
  end

  def unread?(user)
    self.messages.first.user_id != user.id && !self.messages.first.read
  end

  def self.any_unread?(conversations, user)
    unread = false
    conversations.each do |conversation|
      unread = true if conversation.unread?(user)
    end
    unread
  end

end
