class Category < ApplicationRecord
  default_scope { order('position ASC') }
  has_many :jobs
  has_many :users
  has_attached_file :image
  validates_attachment :image, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }

  validates :name, presence: true, uniqueness: true
  
end
