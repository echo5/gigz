class JobApplication < ApplicationRecord
  belongs_to :job
  belongs_to :applicant, class_name: 'User'
  validate :one_per_user

  def one_per_user
    if JobApplication.where(applicant_id: self.applicant_id).where(job_id: self.job_id).count > 0
      errors.add(:base,"You've already applied to this job.")
    end
  end

end
