class PortfolioItem < ApplicationRecord
  belongs_to :user
  has_attached_file :image, :styles => {
  :medium  => "400x300#" }
  validates_attachment :image, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }

end
