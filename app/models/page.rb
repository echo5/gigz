class Page < ApplicationRecord
  validates_presence_of :slug
  
  enum status: {
    draft: 0,
    published: 1,
  }

end
