class Job < ApplicationRecord
  belongs_to :poster, class_name: 'User'
  belongs_to :category
  has_many :applications, class_name: 'JobApplication', dependent: :destroy
  has_many :applicants, through: :applications
  validates_presence_of :job_type, :title, :description, :category, :poster
  validate :valid_credits
  after_create :reduce_credits

  enum job_type: {
    fulltime: 0,
    freelance: 1,
	}
  
  def valid_credits
    if self.job_type == 'fulltime' && self.poster.fulltime_postings_remaining < 1
      errors.add(:base,"You don't have enough full-time posting credits to post a new job.  Please purchase more to create a job.")
    end
    if self.job_type == 'freelance' && self.poster.freelance_postings_remaining < 1
      errors.add(:base,"You don't have enough freelance posting credits to post a new job.  Please purchase more to create a job.")
    end
  end

  def has_applied?(user)
    applications = self.applications.where(applicant_id: user.id)
    return applications.count > 0
  end

  def reduce_credits
    self.poster.fulltime_postings_remaining -= 1 if self.job_type == 'fulltime'
    self.poster.freelance_postings_remaining -= 1 if self.job_type == 'freelance'
    self.poster.save
  end

  # Scopes
  include Filterable
  scope :title, -> (title) { where('lower(title) like lower(?)', "#{title}%") }
  scope :category, -> (category) { where category: category }
  scope :job_type, -> (job_type) { where job_type: job_type }
  scope :country, -> (country) { where country: country }
  default_scope { order('created_at DESC') }
  self.per_page = 20

end
