class PostComment < ApplicationRecord
  belongs_to :user
  belongs_to :post
	validates_presence_of :post_id, :user_id, :content
	belongs_to :parent, class_name: 'PostComment', foreign_key: :parent_id
	has_many :children, -> { order(created_at: :desc) }, class_name: 'PostComment', foreign_key: :parent_id, :dependent => :destroy
	has_many :approved_children, -> { where(status: 'approved').order(created_at: :asc) }, class_name: 'PostComment', foreign_key: :parent_id, :dependent => :destroy
  scope :approved_root_comments, -> { where(parent_id: nil).where(status: 'approved').order(created_at: :desc) }
  
  enum status: {
    pending: 0,
    rejected: 1,
    approved: 2,
  }
end
