class Product < ApplicationRecord
  has_attached_file :image
  validates_attachment :image, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
  
	enum status: {
    draft: 0,
    published: 1,
  }
  
  enum job_type: {
    fulltime: 0,
    freelance: 1,
  }
end
