class Message < ApplicationRecord
  belongs_to :conversation
  belongs_to :user
  validates_presence_of :body, :conversation_id, :user_id
  after_create :set_last_message_at
  default_scope { order('created_at DESC') }
  self.per_page = 20

  attr_accessor :recipient_id

  def message_time
   created_at.strftime("%m/%d/%y at %l:%M %p")
  end

  def set_last_message_at
    self.conversation.last_message_at = Time.now
    self.conversation.save
  end
  
end
