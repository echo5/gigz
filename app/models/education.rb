class Education < ApplicationRecord
  belongs_to :user

  default_scope { order('end_year ASC') }

  
end
