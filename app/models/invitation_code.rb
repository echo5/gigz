class InvitationCode < ApplicationRecord
  belongs_to :user
  belongs_to :owner, class_name: "User"

  enum role: User.roles

end
