class Coupon < ApplicationRecord
  def get_discount(amount)
    if value_discount
      final_amount = amount - value_discount
    end
    if percentage_discount
      final_amount = final_amount * (1 - (percentage_discount/100) )
    end
    final_amount > 0 ? final_amount : 0
  end
end
