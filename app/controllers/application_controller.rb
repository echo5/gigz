class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery with: :exception
  before_action :require_login, unless: :devise_controller?
  before_action :get_conversations
  after_action :verify_authorized, except: :index, unless: :devise_controller?
  after_action :verify_policy_scoped, only: :index, unless: :devise_controller?
  skip_after_action :verify_authorized, if: lambda { controller_name == 'pictures' }
  skip_after_action :verify_policy_scoped, if: lambda { controller_name == 'pictures' }
  before_filter :configure_permitted_parameters, if: :devise_controller?
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  before_filter :store_current_location, :unless => :devise_controller?
  
  def store_current_location
    store_location_for(:user, request.url)
  end

  protected
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :email, :country, :category_id, :terms, :invitation_code])
    end

  private

    def require_login
      if !current_user
        redirect_to new_user_registration_path
      end
    end

    def user_not_authorized
      if !current_user
        flash[:alert] = "Please log in to continue."
        redirect_to new_user_session_path      
      else
        flash[:alert] = "You are not authorized to perform this action."
        redirect_to(request.referrer || root_path)
      end
    end

    def get_conversations
      if current_user
        @conversations = current_user.conversations.order('last_message_at DESC').limit(10)
        @unread_conversations = Conversation.any_unread?(@conversations, current_user)
      end
    end

end
