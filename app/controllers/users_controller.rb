class UsersController < ApplicationController
  before_action :set_user, except: [:index]

  # GET /users
  # GET /users.json
  def index
    @users = policy_scope(User).filter(filtering_params(params)).paginate(:page => params[:page])
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @connections = @user.friends.limit(10)
    @message = Message.new
    @endorsement = Endorsement.new
  end

  # GET /users/1/connections
  # GET /users/1/connections.json
  def connections
    @connections = @user.friends.limit(10).paginate(:page => params[:page])    
  end

  # GET /users/1/edit
  def edit
  end

  # GET /users/1/connect  
  def connect
    current_user.friend_request(@user)
    flash[:success] = 'Connection request sent.'
    redirect_to :back
  end

  # GET /users/1/accept_connection  
  def accept_connection
    current_user.accept_request(@user)
    flash[:success] = 'Connection accepted.'
    redirect_to :back
  end

  # GET /users/1/remove_connection  
  def remove_connection
    current_user.remove_friend(@user)
    flash[:success] = 'Connection removed.'
    redirect_to :back
  end

  # GET /users/1/decline_connection    
  def decline_connection
    current_user.decline_request(@user)
    flash[:success] = 'Connection declined.'
    redirect_to :back
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User profile successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.includes(:portfolio_items,:category,:connections, job_applications: :job).find(params[:id])
      authorize @user
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.fetch(:user, {}).permit(:name,:country,:tagline,:elevator_pitch,:email,:category_id,:avatar,educations_attributes: [:id, :school, :degree, :start_year, :end_year, :_destroy], experiences_attributes: [:id, :company, :title, :start_year, :end_year, :description, :_destroy], portfolio_items_attributes: [:id, :url, :description, :image, :_destroy])
    end

    # A list of the param names that can be used for filtering
    def filtering_params(params)
      params.slice(:uname, :category, :country)
    end
end
