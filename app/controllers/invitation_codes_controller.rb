class InvitationCodesController < ApplicationController
  skip_before_filter :verify_authenticity_token
  skip_before_filter :require_login

  # GET /invitation_codes/check
  def check
    skip_authorization

    errors = []
    if params[:user][:terms] != '1'
      errors << 'Please accept the terms and conditions'
    end

    if !params[:user][:category_id] 
      errors << 'Please select a category for your account'
    end

    code = InvitationCode.find_by_code(params[:user][:invitation_code])
    if !code 
      errors << 'Invitation code does not exist'
    elsif !code.user_id.nil?
      errors << 'Invitation code has already been used'
    end

    if errors.any?
      flash[:error] = errors.join("<br>")
      redirect_to :back
    else
      session[:invitation_code] = params[:user][:invitation_code]
      session[:category_id] = params[:user][:category_id]
      redirect_to params[:redirect_to]
    end
  end

end
