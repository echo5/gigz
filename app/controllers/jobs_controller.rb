class JobsController < ApplicationController
  before_action :set_job, except: [:index, :new, :my, :create]

  # GET /jobs
  # GET /jobs.json
  def index
    @jobs = policy_scope(Job).filter(filtering_params(params)).paginate(:page => params[:page])
  end

  # GET /jobs/1
  # GET /jobs/1.json
  def show
  end

  # GET /jobs/new
  def new
    @job = Job.new
    authorize @job
  end

  # GET /jobs/1/edit
  def edit
  end

  def my
    authorize Job
    @jobs = current_user.jobs.paginate(:page => params[:page])
  end

  # GET /jobs/1/apply
  def apply
    @application = @job.applications.create(applicant: current_user)
    if @application.errors.any?
      flash[:error] = @application.errors.full_messages.join('<br>')
    else
      flash[:success] = 'Applied for job.'
    end
    redirect_to :back
  end

  # POST /jobs
  # POST /jobs.json
  def create
    @job = Job.new(job_params)
    @job.poster = current_user
    authorize @job
    if @job.save
      redirect_to @job, notice: 'Job was successfully created.'
    else
      flash[:error] = @job.errors.full_messages.join('<br>')
      render :new
    end
  end

  # PATCH/PUT /jobs/1
  # PATCH/PUT /jobs/1.json
  def update
    respond_to do |format|
      if @job.update(job_params)
        format.html { redirect_to @job, notice: 'Job was successfully updated.' }
        format.json { render :show, status: :ok, location: @job }
      else
        format.html { render :edit }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    @job.destroy
    respond_to do |format|
      format.html { redirect_to jobs_url, notice: 'Job was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Job.find(params[:id])
      authorize @job
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_params
      params.require(:job).permit(:job_type, :title, :country, :description, :category_id)
    end

    # A list of the param names that can be used for filtering
    def filtering_params(params)
      params.slice(:title, :category, :country, :job_type)
    end
end
