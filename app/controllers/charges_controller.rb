class ChargesController < ApplicationController

  def check_coupon
    authorize Charge
    @coupon = Coupon.find_by_code(params[:couponCode])
    render json: @coupon
  end

  def create
    authorize Charge    
    # Amount in cents
    @product = Product.find(params[:productId])
    @quantity = params[:quantity].to_i
    @amount = @quantity * @product.price
    @final_amount = @amount
  
    @code = params[:couponCode]
  
    if !@code.blank?
      @coupon = Coupon.find_by_code(params[:couponCode])    
  
      if @coupon.nil?
        flash[:error] = 'Coupon code is not valid or expired.'
        redirect_to new_charge_path
        return
      else
        @final_amount = @coupon.get_discount(@amount).to_i
      end
  
      charge_metadata = {
        :coupon_code => @code,
        :coupon_value_discount => (@coupon.value_discount).to_s,
        :coupon_percentage_discount => (@coupon.percentage_discount).to_s + "%"
      }
    end
  
    charge_metadata ||= {}
  
    customer = Stripe::Customer.create(
      :email => params[:stripeEmail],
      :source  => params[:stripeToken]
    )
    @charge = Stripe::Charge.create(
      :customer    => customer.id,
      :amount      => @final_amount,
      :description => 'Gigz Stripe customer',
      :currency    => 'usd',
      :metadata    => charge_metadata
    )

    
    if @charge.status == 'succeeded'
      @sale = Sale.create(
        stripe_customer_id: customer.id,
        customer_id: current_user.id,
        product_id: @product.id,
        amount: @final_amount,
        quantity: @quantity,
        currency: 'usd',
      )
      @sale.coupon_id = @coupon.id if !@coupon.nil?
      @sale.save
      current_user.freelance_postings_remaining += @quantity if @product.job_type == 'freelance'
      current_user.full_time_postings_remaining += @quantity if @product.job_type == 'full_time'
      current_user.save
      flash[:success] = 'Order was successful.'
    end
  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to new_charge_path
  end
end
