class PagesController < ApplicationController
  before_action :set_page
  skip_before_filter :require_login

  def show
  end

  private
    def set_page
      @page = Page.find_by_slug!(params[:slug])
      authorize @page
    end

end
