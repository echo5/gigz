class MessagesController < ApplicationController
  before_action :set_message, only: [:show, :edit, :update, :destroy]

  # GET /messages/new
  def new
    @message = Message.new
  end

  # POST /messages
  # POST /messages.json
  def create
    @recipient = User.find(params[:message][:recipient_id])
    @conversation = Conversation.between(current_user,@recipient).first_or_create(sender_id: current_user.id, recipient_id: @recipient.id)
    @message = @conversation.messages.build(user: current_user, body: params[:message][:body])
    authorize @message

    respond_to do |format|
      if @message.save
        format.html { redirect_to @conversation, notice: 'Message sent.' }
        format.json { render :show, status: :created, location: @message }
      else
        format.html { render :new }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.find(params[:id])
      authorize @message
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
      params.fetch(:message, {}).permit(:body)
    end
end
