class OmniauthCallbacksController < Devise::OmniauthCallbacksController
    
  def facebook
    begin
        @user = User.from_omniauth(request.env["omniauth.auth"], session[:role], session[:invitation_code], session[:category_id])
        sign_in_and_redirect @user
      rescue => ex
        flash[:error] = ex.message
        redirect_to new_user_registration_path
    end
  end

  def google_oauth2
    begin
      @user = User.from_omniauth(request.env["omniauth.auth"], session[:role], session[:invitation_code], session[:category_id])
      sign_in_and_redirect @user
    rescue => ex
      flash[:error] = ex.message
      redirect_to new_user_registration_path
    end
  end

#   def linkedin
#       @user = User.from_omniauth(request.env["omniauth.auth"])
#       sign_in_and_redirect @user
#   end
end