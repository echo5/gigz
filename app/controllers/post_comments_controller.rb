class PostCommentsController < ApplicationController
  before_action :set_post_comment, only: [:show, :edit, :update, :destroy]

  # POST /post_comments
  # POST /post_comments.json
  def create
    @post_comment = PostComment.new(post_comment_params)
    @post_comment.user = current_user
    @post_comment.status = 'approved'
    authorize @post_comment

    if @post_comment.save
      flash[:success] = 'Post comment was successfully created.'
    else
      flash[:error] = @post_comment.errors.full_messages.join('<br>')
    end
    redirect_to :back
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post_comment
      @post_comment = PostComment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_comment_params
      params.require(:post_comment).permit(:parent_id, :post_id, :content)
    end
end
