class ConnectionsController < ApplicationController

  # GET /connections
  def index
    authorize Connection
    @connections = current_user.friends.paginate(:page => params[:page])
    @requested_connections = current_user.requested_friends.paginate(:page => params[:page])
    skip_policy_scope
  end

end