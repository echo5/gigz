class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy, :like, :unlike]
  skip_before_filter :require_login, only: [:show]

  # POST /posts/1/like  
  def like
    @post.liked_by current_user
  end

  # POST /posts/1/unlike  
  def unlike
    @post.unliked_by current_user
  end

  # GET /posts
  # GET /posts.json
  def index
    @posts = policy_scope(Post).includes(:user, :comments).order(created_at: :desc).paginate(:page => params[:page])
    @connections = current_user.connections
    @invitation_code = current_user.get_available_invitation_code
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @post_comment = PostComment.new    
  end

  # GET /posts/new
  def new
    @post = Post.new
    authorize @post
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)
    @post.slug = post_params[:title].parameterize
    @post.status = 'published'
    @post.user = current_user
    authorize @post

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
      authorize @post
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:title, :description, :image, :content)
    end
end
