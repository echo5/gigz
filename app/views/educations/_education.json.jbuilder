json.extract! education, :id, :school, :degree, :from_year, :to_year, :created_at, :updated_at
json.url education_url(education, format: :json)
