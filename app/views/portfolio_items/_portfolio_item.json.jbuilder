json.extract! portfolio_item, :id, :user_id, :url, :description, :image, :created_at, :updated_at
json.url portfolio_item_url(portfolio_item, format: :json)
