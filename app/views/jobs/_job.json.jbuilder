json.extract! job, :id, :poster_id_id, :description, :category, :created_at, :updated_at
json.url job_url(job, format: :json)
