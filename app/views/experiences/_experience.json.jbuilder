json.extract! experience, :id, :company, :title, :from_year, :to_year, :description, :user_id, :created_at, :updated_at
json.url experience_url(experience, format: :json)
