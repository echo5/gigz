class InvitationCodeMailer < ApplicationMailer
  
  def send_codes(user, invitation_codes)
    @user = user
    @invitation_codes = invitation_codes
    mail(to: @user.email, subject: 'Our Gigz Invitation Codes')
  end

end
