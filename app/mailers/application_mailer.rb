class ApplicationMailer < ActionMailer::Base
  default from: 'community@ourgigz.com'
  layout 'mailer'
end
