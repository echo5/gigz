# Preview all emails at http://localhost:3000/rails/mailers/invitation_code_mailer
class InvitationCodeMailerPreview < ActionMailer::Preview
  def send_codes
    InvitationCodeMailer.send_codes(User.first, InvitationCode.limit(10))
  end
end
