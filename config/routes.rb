Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, :controllers => { :omniauth_callbacks => "omniauth_callbacks", confirmations: "confirmations" }
  resources :users, except: [:new] do
    get :connections, on: :member
    get :connect, on: :member
    get :accept_connection, on: :member
    get :remove_connection, on: :member
    get :decline_connection, on: :member
    post :message, on: :member
  end
  resources :connections, only: [:index]
  resources :conversations
  resources :messages
  resources :endorsements
  resources :products, only:[:index, :show]
  resources :charges, only: [:create] do 
    post :check_coupon, on: :collection
  end
  resources :posts, path: 'feed' do
    post :like, on: :member
    post :unlike, on: :member
  end
  resources :post_comments
  match "/invitation_codes/check" => "invitation_codes#check", via: [:post]
  get "/invitation_codes/check", to: redirect('/')
  

  resources :categories, only: [:index, :show]
  resources :job_applications
  resources :jobs do
    get :apply, on: :member
    get :my, on: :collection
  end
  
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  match "/:slug" => "pages#show", via: [:get], as: 'show_page'
  root 'posts#index'
  
end
