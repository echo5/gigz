module IOStream
  def to_tempfile
    name = respond_to?(:original_filename) ? original_filename : (respond_to?(:path) ? path : "stream")
    tempfile = Tempfile.new(["stream", File.extname(name)])
    tempfile.binmode
    self.stream_to(tempfile)
  end
end