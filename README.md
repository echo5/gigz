# Gigz Social Network

A social network built in Rails for creative professionals with chat modules and third-party social network syncing.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Ruby 2.3.0
* MySQL

### Installing

Run bundler like any other Rails app.

```
bundle install
```

Create a dotenv file for with the relevant information

```
SECRET_KEY_BASE=
STRIPE_SECRET_KEY=
STRIPE_PUBLISHABLE_KEY=
FACEBOOK_APP_ID=
FACEBOOK_APP_SECRET=
LINKEDIN_APP_ID=
LINKEDIN_APP_SECRET=
GOOGLE_CLIENT_ID=
GOOGLE_CLIENT_SECRET=
MYSQL_DATABASE=
MYSQL_USERNAME=
MYSQL_PASSWORD=
HOST=
```

## Running the tests

Tests can be run with:

```
bundle exec rake test
```

## Deployment

To setup initial shared and versioned folders for deployment, install Mina and run Mina's init command.

```
gem install mina
mina init
```

Once setup, the site can be pushed with ```mina deploy``` which will build the application, run all tests, and symlink shared files to the new version.