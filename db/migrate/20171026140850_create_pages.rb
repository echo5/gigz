class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
			t.string :title
			t.string :slug, :unique => true
			t.string :description
			t.text :content
      t.integer :status, :default => 1, :null => false

      t.timestamps
    end
  end
end