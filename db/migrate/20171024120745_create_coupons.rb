class CreateCoupons < ActiveRecord::Migration[5.0]
  def change
    create_table :coupons do |t|
      t.string :code
      t.string :description
      t.integer :value_discount
      t.integer :percentage_discount
      
      t.timestamps
    end
  end
end
