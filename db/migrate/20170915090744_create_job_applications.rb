class CreateJobApplications < ActiveRecord::Migration[5.0]
  def change
    create_table :job_applications do |t|
      t.references :job, foreign_key: true
      t.references :applicant, references: :users

      t.timestamps
    end
  end
end
