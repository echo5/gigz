class CreatePostComments < ActiveRecord::Migration[5.0]
  def change
    create_table :post_comments do |t|
      t.references :user, foreign_key: true
      t.references :post, foreign_key: true
      t.references :parent, index: true
      t.text :content
      t.integer :status

      t.timestamps
    end
  end
end
