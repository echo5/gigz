class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
		  t.string :name
		  t.string :tagline
      t.string :country
		  t.text :elevator_pitch
		  t.integer :role, :default => 0
		  t.references :category
		  t.integer :fulltime_postings_remaining, :default => 0, :null => false
		  t.integer :freelance_postings_remaining, :default => 0, :null => false
      t.attachment :avatar			
      t.string :provider
      t.string :uid

      t.string :confirmation_token, index: true
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      
      t.timestamps
    end
  end
end
