class CreateSales < ActiveRecord::Migration[5.0]
  def change
    create_table :sales do |t|
      t.string :stripe_customer_id
      t.references :customer, references: :users
      t.references :product
      t.integer :amount
      t.integer :quantity
      t.integer :coupon_id
      t.string :currency

      t.timestamps
    end
  end
end
