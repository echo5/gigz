class CreateExperiences < ActiveRecord::Migration[5.0]
  def change
    create_table :experiences do |t|
      t.string :company
      t.string :title
      t.integer :start_year
      t.integer :end_year
      t.text :description
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
