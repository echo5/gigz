class CreateEducations < ActiveRecord::Migration[5.0]
  def change
    create_table :educations do |t|
      t.string :school
      t.string :degree
      t.integer :start_year
      t.integer :end_year
      t.references :user, foreign_key: true
      
      t.timestamps
    end
  end
end
