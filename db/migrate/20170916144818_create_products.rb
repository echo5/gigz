class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.integer :price
      t.string :permalink
      t.string :name
      t.text :description
      t.integer :status
      t.integer :job_type
      t.attachment :image

      t.timestamps
    end
  end
end
