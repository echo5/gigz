class CreateJobs < ActiveRecord::Migration[5.0]
  def change
    create_table :jobs do |t|
      t.references :poster, references: :users
      t.string :title
      t.text :description
      t.references :category
      t.integer :job_type
      t.string :country

      t.timestamps
    end
  end
end
