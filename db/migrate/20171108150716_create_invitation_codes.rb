class CreateInvitationCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :invitation_codes do |t|
      t.string :code, unique: true
      t.integer :role
      t.references :owner, references: :users
      t.references :user
      t.timestamps
    end
  end
end
