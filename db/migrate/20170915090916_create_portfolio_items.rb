class CreatePortfolioItems < ActiveRecord::Migration[5.0]
  def change
    create_table :portfolio_items do |t|
      t.references :user, foreign_key: true
      t.string :url
      t.text :description
      t.attachment :image

      t.timestamps
    end
  end
end
