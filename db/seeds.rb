# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Categories
categories = [
  'Account Managers',
  'Artists & Entertainers',
  'Data Scientists',
  'Digital Marketing',
  'Film & Video Producers',
  'Product Managers',
  'Web & Mobile Developers',
  'Writers & Translators '
]
categories.each do |category|
  Category.create(name: category)
end

users = [
  ['Joshua', 'admin', 'joshua@echo5web.com'],
  ['Morgana','admin', 'morgana@echo5web.com'],
  ['Daniel', 'admin', 'daniel@echo5web.com'],
  ['Tejas', 'admin', 'tejas.shah91@gmail.com'],
  ['Joe Biden', 'talent', 'joebiden@example.com'],
  ['Larry Bird', 'talent', 'larrybird@example.com'],
  ['Bob Saget', 'influencer', 'bobsaget@example.com'],
]

users.each do |name, role, email|
  # name.gsub(/[\s,]/ ,"").downcase + '@example.com'
  user = User.create( name: name, email: email, role: role, password: 'password', password_confirmation: 'password', category_id: 3, terms: true)
  3.times do |index|
    PortfolioItem.create(
      description: 'Cool item ' + index.to_s,
      url: 'http://google.com',
      image: 'http://lorempixel.com/400/200',
      user_id: user.id
    )
  end
  
end


# Products
products = [
  ['Job Posting', '1', '99'],
  ['Freelance Posting', '1', '49']
]
products.each do |name, units, price|
  Product.create(name: name, permalink: name.parameterize, units: units, price: price)
end



# Jobs
jobs = [
  ['Senior Developer', 'Are you l33t enough?  Swordfish wannabes welcome.'],
  ['Sketch Graphic Designer', 'Looking for someone who knows how to select layers in Sketch.']
]
poster = User.find_by_name!('Bob Saget')
category = Category.find(3)
jobs.each do |title, description|
  Job.create(title: title, description: description, poster_id: poster.id, category: category, job_type: 'fulltime')
end